function initializeUserApp() {
    return new Vue({
        el: '#app',
        data: {
            trips: [
                //     { id: 1, name: "Trip to Adelaide" },
                //     { id: 2, name: "Kangaroo Island Adventure" }
            ],
            feedback: [
                //     {
                //         id: 1, tripId: 1, author: "John Doe", rating: 5, message: "Amazing experience!", date: new Date()
                //     },
                //     {
                //         id: 2, tripId: 2, author: "Jane Smith", rating: 4, message: "Loved the wildlife.", date: new Date()
                //     }
            ],
            selectedTripId: '',
            feedbackMessage: '',
            anonymous: false,
            rating: 1,
            userId: 1
        },
        computed: {
            filteredFeedback() {
                return this.feedback.filter((f) => f.tripId === parseInt(this.selectedTripId, 10));
            }
        },
        methods: {
            submitFeedback() {
                if (!this.selectedTripId) return alert('Please select a trip');
                if (!this.feedbackMessage.trim()) return alert('Please enter your feedback');

                const hasSubmitted = this.feedback.some((f) => f.tripId === parseInt(this.selectedTripId, 10) && f.userId === this.userId);
                if (hasSubmitted) return alert('You have already submitted feedback for this trip');

                const newFeedback = {
                    tripId: this.selectedTripId,
                    author: this.anonymous ? 'Anonymous' : 'Current User',
                    message: this.feedbackMessage,
                    rating: this.rating,
                    date: new Date().toISOString(),
                    userId: this.userId
                };

                fetch('/api/feedback', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(newFeedback)
                })
                    .then(response => response.json())
                    .then(() => {
                        this.fetchFeedback();
                    })
                    .catch(error => {
                        console.error("Error submitting feedback:", error);
                    });
            },
            getTripName(tripId) {
                const trip = this.trips.find((t) => t.id === tripId);
                return trip ? trip.name : "Unknown Trip";
            },
            fetchFeedback() {
                fetch('/api/feedback')
                    .then(response => response.json())
                    .then(data => {
                        this.feedbackMessage = '';
                        this.rating = 1;
                        this.feedback = data;
                    })
                    .catch(error => {
                        console.error("Error fetching feedback:", error);
                    });
            }
        },
        mounted() {
            fetch('/api/trips')
                .then(response => response.json())
                .then(data => {
                    this.trips = data;
                    this.loading = false;
                })
                .catch(error => {
                    console.error("Error fetching trips:", error);
                    this.loading = false;
                });

            fetch('/api/feedback')
                .then(response => response.json())
                .then(data => {
                    this.feedback = data;
                })
                .catch(error => {
                    console.error("Error fetching feedback:", error);
                });
        }
    });
}

document.addEventListener('DOMContentLoaded', initializeUserApp);
