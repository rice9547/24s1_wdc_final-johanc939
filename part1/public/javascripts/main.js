new Vue({
    el: '#app',
    methods: {
        goToUserPage() {
            window.location.href = '/user';
        },
        goToManagerPage() {
            window.location.href = '/manager';
        }
    }
});
