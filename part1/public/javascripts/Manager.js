function initializeManagerApp() {
    return new Vue({
        el: '#app',
        data: {
            trips: [
                // { id: 1, name: "Trip to Adelaide" },
                // { id: 2, name: "Kangaroo Island Adventure" }
            ],
            feedback: [
                // {
                //     id: 1, tripId: 1, author: "John Doe", rating: 5, message: "Amazing experience!", date: new Date()
                // },
                // {
                //     id: 2, tripId: 2, author: "Jane Smith", rating: 4, message: "Loved the wildlife.", date: new Date()
                // }
            ]
        },
        computed: {
            sortedFeedback() {
                return this.feedback.sort((a, b) => new Date(b.date) - new Date(a.date));
            }
        },
        methods: {
            getTripName(tripId) {
                const trip = this.trips.find((t) => t.id === tripId);
                return trip ? trip.name : "Unknown Trip";
            }
        },
        mounted() {
            fetch('/api/trips')
                .then(response => response.json())
                .then(data => {
                    this.trips = data;
                    this.loading = false;
                })
                .catch(error => {
                    console.error("Error fetching trips:", error);
                    this.loading = false;
                });

            fetch('/api/feedback')
                .then(response => response.json())
                .then(data => {
                    this.feedback = data;
                })
                .catch(error => {
                    console.error("Error fetching feedback:", error);
                });
        }
    });
}

document.addEventListener('DOMContentLoaded', initializeManagerApp);
