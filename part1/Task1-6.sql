SELECT
    Trips.Destination,
    Trips.Duration,
    Trips.StartDate,
    SUM(Bookings.NumberOfTravelers) AS TotalTravelers
FROM
    Bookings
JOIN
    Trips ON Bookings.TripID = Trips.TripID
GROUP BY
    Trips.Destination,
    Trips.Duration,
    Trips.StartDate
ORDER BY
    TotalTravelers DESC
LIMIT 10;