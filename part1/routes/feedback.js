var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    req.pool.getConnection(function (err, connection) {
        if (err) throw err;
        const query = `
            SELECT Feedback.FeedbackID AS id,
                   Feedback.TripID AS tripId,
                   Feedback.Rating AS rating,
                   Feedback.Comment AS message,
                   Feedback.Date AS date,
                   Users.Username AS author
            FROM Feedback 
            JOIN Users ON Feedback.UserID = Users.UserID;
        `;
        connection.query(query, function (error, results, fields) {
            connection.release();
            if (error) throw error;
            res.json(results);
        });
    });
});

router.post('/', function (req, res, next) {
    req.pool.getConnection(function (err, connection) {
        if (err) throw err;
        const feedback = {
            TripID: req.body.tripId,
            UserID: req.body.userId,
            Rating: req.body.rating,
            Comment: req.body.message,
            Date: new Date(req.body.date)
        };
        connection.query('INSERT INTO Feedback SET ?', feedback, function (error, results, fields) {
            connection.release();
            if (error) throw error;
            res.json({ ...feedback, id: results.insertId });
        });
    });
});

module.exports = router;
