var express = require('express');
var path = require('path');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/user', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..', 'public', 'User.html'));
});


router.get('/manager', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..', 'public', 'Manager.html'));
});

module.exports = router;
