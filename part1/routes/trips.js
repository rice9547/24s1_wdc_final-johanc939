var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    req.pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query('SELECT TripID AS id, Destination AS name FROM Trips', function (error, results, fields) {
            connection.release();
            if (error) throw error;
            res.json(results);
        });
    });
});

module.exports = router;
