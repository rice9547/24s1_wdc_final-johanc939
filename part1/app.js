var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var mysql = require('mysql');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tripsRouter = require('./routes/trips');
var feedbackRouter = require('./routes/feedback');

var app = express();

var dbConnectionPool = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    multipleStatements: true
});

app.use(function (req, res, next) {
    req.pool = dbConnectionPool;
    next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
    secret: 'a706835de79a2b4e90506f582af3676ac8361521',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));

app.use(function (req, res, next) {
    if ('user' in req.session) {
        res.cookie('role', req.session.user.role);
    } else {
        res.cookie('role', 'anon');
    }
    next();
});

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use('/api/users', usersRouter);
app.use('/api/trips', tripsRouter);
app.use('/api/feedback', feedbackRouter);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

function initialise_db() {
    dbConnectionPool.getConnection(function (err, connection) {
        if (err) {
            console.error("Unable to configure database. Is your database running and has the correct permissions?");
            return;
        }
        var setup = `
        DROP DATABASE IF EXISTS TourismDB;
        CREATE DATABASE TourismDB;
        USE TourismDB;

        CREATE TABLE Users (
            UserID INT AUTO_INCREMENT PRIMARY KEY,
            Username VARCHAR(50) NOT NULL,
            Email VARCHAR(100) NOT NULL UNIQUE,
            Password VARCHAR(100) NOT NULL,
            Role ENUM('user', 'manager') NOT NULL
        );

        CREATE TABLE Trips (
            TripID INT AUTO_INCREMENT PRIMARY KEY,
            Destination VARCHAR(100) NOT NULL,
            Duration VARCHAR(50) NOT NULL,
            StartDate DATE NOT NULL,
            EndDate DATE NOT NULL,
            Description TEXT,
            SlotsAvailable INT NOT NULL,
            ManagerID INT,
            FOREIGN KEY (ManagerID) REFERENCES Users(UserID)
        );

        CREATE TABLE Bookings (
            BookingID INT AUTO_INCREMENT PRIMARY KEY,
            TripID INT,
            UserID INT,
            NumberOfTravelers INT NOT NULL,
            BookingDate DATE NOT NULL,
            FOREIGN KEY (TripID) REFERENCES Trips(TripID),
            FOREIGN KEY (UserID) REFERENCES Users(UserID)
        );

        CREATE TABLE Feedback (
            FeedbackID INT AUTO_INCREMENT PRIMARY KEY,
            TripID INT,
            UserID INT,
            Rating INT CHECK (Rating >= 1 AND Rating <= 5),
            Comment TEXT,
            Date DATE NOT NULL,
            FOREIGN KEY (TripID) REFERENCES Trips(TripID),
            FOREIGN KEY (UserID) REFERENCES Users(UserID)
        );

        INSERT INTO Users (Username, Email, Password, Role) VALUES
        ('manager1', 'manager1@example.com', 'password123', 'manager'),
        ('user1', 'user1@example.com', 'password123', 'user');

        INSERT INTO Trips (Destination, Duration, StartDate, EndDate, Description, SlotsAvailable, ManagerID) VALUES
        ('Kangaroo Island', '3 days 2 nights', '2024-07-01', '2024-07-03', 'A beautiful trip to Kangaroo Island', 20, 1),
        ('Marion Bay', '4 days 3 nights', '2024-08-01', '2024-08-04', 'Explore the scenic Marion Bay', 15, 1);

        INSERT INTO Bookings (TripID, UserID, NumberOfTravelers, BookingDate) VALUES
        (1, 2, 2, '2024-06-20');

        INSERT INTO Feedback (TripID, UserID, Rating, Comment, Date) VALUES
        (1, 2, 5, 'Amazing trip! Highly recommended.', '2024-07-05');
        `;
        connection.query(setup, function (qerr, rows, fields) {
            connection.release(); // release connection
            if (qerr) {
                console.error("Unable to configure database. Is your database running and has the correct permissions?");
                return;
            }
            dbConnectionPool.on('connection', function (conn) {
                conn.query('USE TourismDB;');
            });
        });
    });
}

initialise_db();

module.exports = app;
