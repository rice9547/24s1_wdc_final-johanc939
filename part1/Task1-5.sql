SELECT
    Trips.Destination,
    Trips.Duration,
    Trips.StartDate,
    Bookings.BookingDate
FROM
    Bookings
JOIN
    Trips ON Bookings.TripID = Trips.TripID
WHERE
    Bookings.UserID = [UserID]
    AND Bookings.BookingDate >= CURDATE() - INTERVAL 30 DAY
ORDER BY
    Bookings.BookingDate ASC;