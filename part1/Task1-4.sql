CREATE DATABASE TourismDB;
USE TourismDB;
CREATE TABLE Users (
    UserID INT AUTO_INCREMENT PRIMARY KEY,
    Username VARCHAR(50) NOT NULL,
    Email VARCHAR(100) NOT NULL UNIQUE,
    Password VARCHAR(100) NOT NULL,
    Role ENUM('user', 'manager') NOT NULL
);

CREATE TABLE Trips (
    TripID INT AUTO_INCREMENT PRIMARY KEY,
    Destination VARCHAR(100) NOT NULL,
    Duration VARCHAR(50) NOT NULL,
    StartDate DATE NOT NULL,
    EndDate DATE NOT NULL,
    Description TEXT,
    SlotsAvailable INT NOT NULL,
    ManagerID INT,
    FOREIGN KEY (ManagerID) REFERENCES Users(UserID)
);

CREATE TABLE Bookings (
    BookingID INT AUTO_INCREMENT PRIMARY KEY,
    TripID INT,
    UserID INT,
    NumberOfTravelers INT NOT NULL,
    BookingDate DATE NOT NULL,
    FOREIGN KEY (TripID) REFERENCES Trips(TripID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID)
);

CREATE TABLE Feedback (
    FeedbackID INT AUTO_INCREMENT PRIMARY KEY,
    TripID INT,
    UserID INT,
    Rating INT CHECK (Rating >= 1 AND Rating <= 5),
    Comment TEXT,
    Date DATE NOT NULL,
    FOREIGN KEY (TripID) REFERENCES Trips(TripID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID)
);

INSERT INTO Users (Username, Email, Password, Role) VALUES
('manager1', 'manager1@example.com', 'password123', 'manager'),
('user1', 'user1@example.com', 'password123', 'user');

INSERT INTO Trips (Destination, Duration, StartDate, EndDate, Description, SlotsAvailable, ManagerID) VALUES
('Kangaroo Island', '3 days 2 nights', '2024-07-01', '2024-07-03', 'A beautiful trip to Kangaroo Island', 20, 1),
('Marion Bay', '4 days 3 nights', '2024-08-01', '2024-08-04', 'Explore the scenic Marion Bay', 15, 1);

INSERT INTO Bookings (TripID, UserID, NumberOfTravelers, BookingDate) VALUES
(1, 2, 2, '2024-06-20');

INSERT INTO Feedback (TripID, UserID, Rating, Comment, Date) VALUES
(1, 2, 5, 'Amazing trip! Highly recommended.', '2024-07-05');